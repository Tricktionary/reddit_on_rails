class ApplicationController < ActionController::Base
  http_basic_authenticate_with name: "Andy", password: "1234", except: [:index, :show] #Athentication
  protect_from_forgery with: :exception
end
